# osgi-substitution

OSGi-substitution is an API and OSGi service set to allow service substitution without any restart of the client and without any assumption on external services.

This work was conducted in the [CITI Laboratory](http://www.citi-lab.fr/), part of the [Institut National des Sciences Appliquées de Lyon](http://www.insa-lyon.fr/).

## Build requirements

* Java SE 6+
* Maven 3

## Authors

See the `AUTHORS` file.

## Licensing

    osgi-substitution is free software: you can redistribute it and/or modify  
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or          
    (at your option) any later version.                                        

    osgi-substitution is distributed in the hope that it will be useful,       
    but WITHOUT ANY WARRANTY; without even the implied warranty of             
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
    Lesser GNU General Public License for more details.                        

    You should have received a copy of the Lesser GNU General Public License   
    along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.

