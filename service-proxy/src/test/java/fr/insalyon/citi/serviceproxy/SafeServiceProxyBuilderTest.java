/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.serviceproxy;

import fr.insalyon.citi.serviceproxy.DynamicProxyReference;
import fr.insalyon.citi.serviceproxy.impl.SafeServiceProxyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import static fr.insalyon.citi.serviceproxy.ProxyMode.DISABLED_AFTER_UNREGISTERED;
import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class SafeServiceProxyBuilderTest {

    public interface FakeService {

        public String echo(String str);
    }

    public class FakeServiceImpl implements FakeService {

        @Override
        public String echo(String str) {
            return str;
        }
    }

    private BundleContext bundleContext;
    private SafeServiceProxyBuilder proxyBuilder;

    @Before
    public void setup() throws InvalidSyntaxException, NoSuchMethodException {
        FakeService fakeService = new FakeServiceImpl();

        ServiceReference fakeServiceReference = mock(ServiceReference.class);
        bundleContext = mock(BundleContext.class);

        when(bundleContext.getServiceReference(FakeService.class.getName())).thenReturn(fakeServiceReference);
        when(bundleContext.getServiceReferences(FakeService.class.getName(), "")).thenReturn(new ServiceReference[]{fakeServiceReference});
        when(bundleContext.getService(fakeServiceReference)).thenReturn(fakeService);

        proxyBuilder = new SafeServiceProxyBuilder(bundleContext);
    }

    @Test
    public void smoke() {
        ServiceReference reference = bundleContext.getServiceReference(FakeService.class.getName());
        assertThat(reference, notNullValue());

        FakeService service = proxyBuilder.getService(FakeService.class, DISABLED_AFTER_UNREGISTERED);
        assertThat(service, notNullValue());
        assertThat(service.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)service).isValid(), is(true));
    }

    @Test
    public void verify_service_obtention() throws InvalidSyntaxException {
        FakeService service = proxyBuilder.getService(FakeService.class, DISABLED_AFTER_UNREGISTERED);
        assertThat(service, notNullValue());
        assertThat(service.getClass().getName(), is(not(FakeServiceImpl.class.getName())));
        assertThat(service.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)service).isValid(), is(true));

        service = proxyBuilder.getFirstServiceMatching(FakeService.class, "", DISABLED_AFTER_UNREGISTERED);
        assertThat(service, notNullValue());
        assertThat(service.getClass().getName(), is(not(FakeServiceImpl.class.getName())));
        assertThat(service.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)service).isValid(), is(true));
    }
}
