/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.serviceproxy.impl;

import fr.insalyon.citi.serviceproxy.*;
import org.osgi.framework.*;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashSet;
import java.util.Set;

import static fr.insalyon.citi.serviceproxy.ProxyMode.DISABLED_AFTER_UNREGISTERED;
import static fr.insalyon.citi.serviceproxy.ProxyMode.RELOAD_AFTER_UNREGISTERED;
import static java.util.Collections.unmodifiableSet;

public class SafeServiceProxyBuilder implements ServiceProxyBuilder {

    private final BundleContext bundleContext;
    private final Method isValidMethod = DynamicProxyReference.class.getMethod("isValid");

    public SafeServiceProxyBuilder(BundleContext bundleContext) throws NoSuchMethodException {
        super();
        this.bundleContext = bundleContext;
    }

    class FailFastProxy implements InvocationHandler, ServiceListener, DynamicProxyReference {

        private Object service;
        private final ServiceReference serviceReference;

        FailFastProxy(Object service, ServiceReference serviceReference) {
            super();
            this.service = service;
            this.serviceReference = serviceReference;
        }

        @Override
        public synchronized Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.equals(isValidMethod)) {
                return method.invoke(this, args);
            }
            if (service == null) {
                throw new UnregisteredServiceException("The following ServiceReference is no longer valid: " + serviceReference);
            }
            return method.invoke(service, args);
        }

        @Override
        public synchronized void serviceChanged(ServiceEvent event) {
            if (event.getServiceReference().compareTo(serviceReference) == 0 && event.getType() == ServiceEvent.UNREGISTERING) {
                bundleContext.removeServiceListener(this);
                service = null;
            }
        }

        @Override
        public synchronized boolean isValid() {
            return (service != null);
        }
    }

    class ReloadingProxy implements InvocationHandler, ServiceListener, DynamicProxyReference {

        private final String clazz;
        private final String filter;
        private Object service = null;
        private ServiceReference serviceReference = null;

        ReloadingProxy(String clazz, String filter) {
            super();
            this.clazz = clazz;
            this.filter = filter;
        }

        @Override
        public synchronized Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.equals(isValidMethod)) {
                return method.invoke(this, args);
            }
            if (service == null) {
                ServiceReference[] references = bundleContext.getAllServiceReferences(clazz, filter);
                if (references == null || references.length == 0) {
                    throw new UnregisteredServiceException("A service could not be found for " + clazz + " / " + filter);
                }
                serviceReference = references[0];
                service = bundleContext.getService(serviceReference);
                bundleContext.addServiceListener(this);
            }
            return method.invoke(service, args);
        }

        @Override
        public synchronized void serviceChanged(ServiceEvent event) {
            if (serviceReference != null) {
                if (event.getServiceReference().compareTo(serviceReference) == 0 && event.getType() == ServiceEvent.UNREGISTERING) {
                    service = null;
                    serviceReference = null;
                    bundleContext.removeServiceListener(this);
                }
            }
        }

        @Override
        public synchronized boolean isValid() {
            return (service != null);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T getService(Class<T> clazz, ServiceReference reference, String filter, ProxyMode proxyMode) {
        InvocationHandler invocationHandler = null;
        ServiceListener serviceListener = null;

        T service = (T) bundleContext.getService(reference);

        if (DISABLED_AFTER_UNREGISTERED.equals(proxyMode)) {
            FailFastProxy failFastProxy = new FailFastProxy(service, reference);
            invocationHandler = failFastProxy;
            serviceListener = failFastProxy;
        } else if (RELOAD_AFTER_UNREGISTERED.equals(proxyMode)) {
            ReloadingProxy reloadingProxy = new ReloadingProxy(clazz.getName(), filter);
            invocationHandler = reloadingProxy;
            serviceListener = reloadingProxy;
        }

        try {
            bundleContext.addServiceListener(serviceListener, "(objectclass=" + clazz.getName() + ")");
        } catch (InvalidSyntaxException ignored) {
        }
        return (T) Proxy.newProxyInstance(
                service.getClass().getClassLoader(),
                new Class[]{clazz, DynamicProxyReference.class},
                invocationHandler);
    }

    @Override
    public <T> T getService(Class<T> clazz, ServiceReference reference, ProxyMode proxyMode) {
        return this.getService(clazz, reference, null, proxyMode);
    }

    @Override
    public <T> T getService(Class<T> clazz, ProxyMode proxyMode) {
        return this.getService(clazz, bundleContext.getServiceReference(clazz.getName()), proxyMode);
    }

    @Override
    public <T> T getFirstServiceMatching(Class<T> clazz, String filter, ProxyMode proxyMode) throws InvalidSyntaxException {
        ServiceReference[] references = bundleContext.getServiceReferences(clazz.getName(), filter);
        if (references == null || references.length == 0) {
            return null;
        }
        return this.getService(clazz, references[0], filter, proxyMode);
    }

    class SafeServiceBroker<T> implements ServiceBroker<T>, ServiceListener {

        private final Class<T> clazz;
        private final String filter;

        SafeServiceBroker(Class<T> clazz, String filter) {
            super();
            this.clazz = clazz;
            this.filter = filter;
        }

        private Set<T> references = new HashSet<T>();

        @Override
        @SuppressWarnings("unchecked")
        public synchronized Set<T> currentServices() throws InvalidSyntaxException {
            if (references.isEmpty()) {
                ServiceReference[] allReferences = bundleContext.getAllServiceReferences(clazz.getName(), filter);
                if (allReferences != null) {
                    for (ServiceReference ref : allReferences) {
                        references.add(getService(clazz, ref, DISABLED_AFTER_UNREGISTERED));
                    }
                }
            }
            return unmodifiableSet(references);
        }

        @Override
        public void discard() {
            bundleContext.removeServiceListener(this);
        }

        @Override
        public synchronized void serviceChanged(ServiceEvent serviceEvent) {
            if (serviceEvent.getType() == ServiceEvent.UNREGISTERING) {
                references = new HashSet<T>();
            }
        }
    }

    @Override
    public <T> ServiceBroker<T> getServices(Class<T> clazz, String filter) throws InvalidSyntaxException {
        SafeServiceBroker<T> broker = new SafeServiceBroker<T>(clazz, filter);
        bundleContext.addServiceListener(broker, "(objectclass=" + clazz.getName() + ")");
        return broker;
    }
}
