/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.serviceproxy;

import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

public interface ServiceProxyBuilder {

    public <T> T getService(Class<T> clazz, ServiceReference reference, ProxyMode proxyMode);

    public <T> T getService(Class<T> clazz, ProxyMode proxyMode);

    public <T> T getFirstServiceMatching(Class<T> clazz, String filter, ProxyMode proxyMode)
            throws InvalidSyntaxException;

    public <T> ServiceBroker<T> getServices(Class<T> clazz, String filter)
            throws InvalidSyntaxException;
}
