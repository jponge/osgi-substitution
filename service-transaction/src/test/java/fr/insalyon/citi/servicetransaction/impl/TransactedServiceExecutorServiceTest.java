/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.servicetransaction.impl;

import fr.insalyon.citi.serviceproxy.ProxyMode;
import fr.insalyon.citi.serviceproxy.ServiceBroker;
import fr.insalyon.citi.serviceproxy.ServiceProxyBuilder;
import fr.insalyon.citi.servicetransaction.ServiceInjection;
import fr.insalyon.citi.servicetransaction.TransactedExecution;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.InvalidSyntaxException;

import java.util.*;

import static fr.insalyon.citi.servicetransaction.ServiceInjection.ProxyType.MULTIPLE;
import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class TransactedServiceExecutorServiceTest {

    private ServiceProxyBuilder proxyBuilder;
    private TransactedServiceExecutorService transactedServiceExecutor;

    @Before
    public void prepare() {
        proxyBuilder = mock(ServiceProxyBuilder.class);
        transactedServiceExecutor = new TransactedServiceExecutorService(proxyBuilder);
    }

    @Test
    public void verify_injection() throws InvalidSyntaxException, IllegalAccessException {
        FakeVoidTransactedExecution execution = new FakeVoidTransactedExecution();
        BadInjectionOnSingleReference badInjectionOnSingleReference = new BadInjectionOnSingleReference();
        BadInjectionOnBroker badInjectionOnBroker = new BadInjectionOnBroker();

        when(proxyBuilder.getFirstServiceMatching(List.class, null, ProxyMode.DISABLED_AFTER_UNREGISTERED)).thenReturn(new ArrayList());
        when(proxyBuilder.getServices(String.class, null)).thenReturn(new ServiceBroker<String>() {

            @Override
            public Set<String> currentServices() throws InvalidSyntaxException {
                return new HashSet<String>(asList("a", "b", "c"));
            }

            @Override
            public void discard() {

            }
        });

        transactedServiceExecutor.performInjection(execution);
        assertThat(execution.staysNull, nullValue());
        assertThat(execution.aList, notNullValue());
        assertThat(execution.aList, instanceOf(ArrayList.class));
        assertThat(execution.someStrings, notNullValue());
        assertThat(execution.someStrings.size(), is(3));

        try {
            transactedServiceExecutor.performInjection(badInjectionOnSingleReference);
            throw new RuntimeException("An exception should have been thrown");
        } catch (IllegalArgumentException ignored) {

        }

        try {
            transactedServiceExecutor.performInjection(badInjectionOnBroker);
            throw new RuntimeException("An exception should have been thrown");
        } catch (IllegalArgumentException ignored) {

        }
    }

    private class FakeVoidTransactedExecution implements TransactedExecution<Void> {

        public Object staysNull;

        @ServiceInjection
        public List aList;

        @ServiceInjection(type = String.class, proxyType = MULTIPLE)
        public Set<String> someStrings;

        @Override
        public void prepare() {

        }

        @Override
        public <Void> Void execute() {
            return null;
        }

        @Override
        public void finish() {

        }

        @Override
        public void rollback() {

        }
    }

    private class BadInjectionOnSingleReference implements TransactedExecution<Void> {

        @ServiceInjection(type = Set.class)
        public List aList;

        @Override
        public void prepare() {

        }

        @Override
        public <Void> Void execute() {
            return null;
        }

        @Override
        public void finish() {

        }

        @Override
        public void rollback() {

        }
    }

    private class BadInjectionOnBroker implements TransactedExecution<Void> {

        @ServiceInjection(type = String.class, proxyType = MULTIPLE)
        public List aList;

        @Override
        public void prepare() {

        }

        @Override
        public <Void> Void execute() {
            return null;
        }

        @Override
        public void finish() {

        }

        @Override
        public void rollback() {

        }
    }
}
