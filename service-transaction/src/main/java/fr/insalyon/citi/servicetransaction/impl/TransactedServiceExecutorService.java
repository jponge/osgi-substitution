/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.servicetransaction.impl;

import fr.insalyon.citi.serviceproxy.DynamicProxyReference;
import fr.insalyon.citi.serviceproxy.ServiceBroker;
import fr.insalyon.citi.serviceproxy.ServiceProxyBuilder;
import fr.insalyon.citi.servicetransaction.*;
import org.osgi.framework.InvalidSyntaxException;

import java.lang.reflect.Field;
import java.util.Set;

import static fr.insalyon.citi.servicetransaction.ServiceInjection.ProxyType.SINGLE;

public class TransactedServiceExecutorService implements TransactedServiceExecutor {

    private final ServiceProxyBuilder serviceProxyBuilder;

    public TransactedServiceExecutorService(ServiceProxyBuilder serviceProxyBuilder) {
        this.serviceProxyBuilder = serviceProxyBuilder;
    }

    private boolean isNotValidProxyReference(Field field, Object object) throws IllegalAccessException {
        field.setAccessible(true);
        Object currentValue = field.get(object);
        field.setAccessible(false);
        if (currentValue != null) {
            if (currentValue instanceof DynamicProxyReference) {
                return !(((DynamicProxyReference) currentValue).isValid());
            }
        }
        return true;
    }

    <T> void performInjection(TransactedExecution<T> execution) throws InvalidSyntaxException, IllegalAccessException {
        for (Field field : execution.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ServiceInjection.class) && isNotValidProxyReference(field, execution)) {
                ServiceInjection serviceInjection = field.getAnnotation(ServiceInjection.class);
                Object injectable;
                String filter = serviceInjection.filter();
                if (filter.isEmpty()) {
                    filter = null;
                }
                Class<?> type = serviceInjection.type();
                if (serviceInjection.proxyType().equals(SINGLE)) {
                    if (type == ServiceInjection.class) {
                        type = field.getType();
                    }
                    if (field.getType().isAssignableFrom(type)) {
                        injectable = serviceProxyBuilder.getFirstServiceMatching(type, filter, serviceInjection.proxyMode());
                    } else {
                        throw new IllegalArgumentException("A service of type " + type + " cannot be injected to a reference of type " + field.getType());
                    }
                } else {
                    if (type == ServiceInjection.class) {
                        throw new IllegalArgumentException("A type must be specified when injecting multiple service references");
                    }
                    if (field.getType().isAssignableFrom(Set.class)) {
                        ServiceBroker<?> broker = serviceProxyBuilder.getServices(type, filter);
                        injectable = broker.currentServices();
                        broker.discard();
                    } else {
                        throw new IllegalArgumentException("A MULTIPLE injection must be performed on an instance of java.util.Set");
                    }
                }
                field.setAccessible(true);
                field.set(execution, injectable);
                field.setAccessible(false);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T executeInTransaction(TransactedExecution<T> execution, RetryPolicy retryPolicy) throws TransactedExecutionFailed {
        while (retryPolicy.shouldContinue()) {
            try {
                performInjection(execution);
                try {
                    execution.prepare();
                    Object result = execution.execute();
                    execution.finish();
                    return (T) result;
                } catch (Throwable t) {
                    retryPolicy.notifyOf(t);
                    execution.rollback();
                }
            } catch (InvalidSyntaxException e) {
                throw new TransactedExecutionFailed(e);
            } catch (IllegalAccessException e) {
                throw new TransactedExecutionFailed(e);
            }
        }
        throw new TransactedExecutionFailed("The transacted execution failed");
    }
}
