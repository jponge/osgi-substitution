/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.servicetransaction;

import fr.insalyon.citi.serviceproxy.ProxyMode;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static fr.insalyon.citi.serviceproxy.ProxyMode.DISABLED_AFTER_UNREGISTERED;
import static fr.insalyon.citi.servicetransaction.ServiceInjection.ProxyType.SINGLE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(FIELD)
@Documented
public @interface ServiceInjection {

    Class<?> type() default ServiceInjection.class;

    String filter() default "";

    ProxyType proxyType() default SINGLE;

    ProxyMode proxyMode() default DISABLED_AFTER_UNREGISTERED;

    public static enum ProxyType {SINGLE, MULTIPLE}
}
