/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.osgisubstitution.it;

import fr.insalyon.citi.osgisubstitution.it.echo.EchoService;
import fr.insalyon.citi.osgisubstitution.it.echo.EchoServiceImpl;
import fr.insalyon.citi.servicetransaction.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.*;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;

@RunWith(JUnit4TestRunner.class)
public class ServiceTransactionIntegrationTest {

    @Configuration
    public static Option[] configure() {
        return options(
                felix(),
                equinox(),
                provision(
                        mavenBundle().groupId("fr.insalyon.citi.osgi-substitution").artifactId("service-proxy").versionAsInProject(),
                        mavenBundle().groupId("fr.insalyon.citi.osgi-substitution").artifactId("service-transaction").versionAsInProject()
                )
        );
    }

    @Inject
    BundleContext bundleContext = null;

    TransactedServiceExecutor transactedServiceExecutor;

    @Before
    public void setup() {
        ServiceReference reference = bundleContext.getServiceReference(TransactedServiceExecutor.class.getName());
        transactedServiceExecutor = (TransactedServiceExecutor) bundleContext.getService(reference);
    }

    @Test
    public void smoke() {
        assertThat(bundleContext, notNullValue());
        assertThat(transactedServiceExecutor, notNullValue());
    }

    @Test
    public void basic_test() throws TransactedExecutionFailed {
        final Map<String, Boolean> called = new HashMap<String, Boolean>() {
            {
                this.put("prepare", false);
                this.put("finish", false);
                this.put("rollback", false);
            }
        };
        TransactedExecution<String> echoExecution = new TransactedExecution<String>() {

            @ServiceInjection
            EchoService echoService;

            @Override
            public void prepare() {
                called.put("prepare", true);
            }

            @Override
            public <String> String execute() {
                return (String) echoService.echo("Hello");
            }

            @Override
            public void finish() {
                called.put("finish", true);
            }

            @Override
            public void rollback() {
                called.put("rollback", true);
            }
        };

        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);
            }
        }.start();

        String echo = transactedServiceExecutor.executeInTransaction(echoExecution, new RetryForeverPolicy());

        assertThat(echo, is("Hello"));
        assertThat(called.get("prepare"), is(true));
        assertThat(called.get("finish"), is(true));
        assertThat(called.get("rollback"), is(true));
    }
}
