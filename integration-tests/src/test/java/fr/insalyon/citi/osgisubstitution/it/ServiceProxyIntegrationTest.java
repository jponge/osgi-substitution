/*
 * Copyright (c) 2012 Institut National des Sciences Appliquées de Lyon.
 *
 * This file is part of osgi-substitution.
 *
 * osgi-substitution is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * osgi-substitution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Lesser GNU General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with osgi-substitution. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.citi.osgisubstitution.it;

import fr.insalyon.citi.osgisubstitution.it.echo.AnotherEchoServiceImpl;
import fr.insalyon.citi.osgisubstitution.it.echo.EchoService;
import fr.insalyon.citi.osgisubstitution.it.echo.EchoServiceImpl;
import fr.insalyon.citi.serviceproxy.DynamicProxyReference;
import fr.insalyon.citi.serviceproxy.ServiceBroker;
import fr.insalyon.citi.serviceproxy.ServiceProxyBuilder;
import fr.insalyon.citi.serviceproxy.UnregisteredServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Inject;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static fr.insalyon.citi.serviceproxy.ProxyMode.*;
import static fr.insalyon.citi.serviceproxy.ProxyMode.RELOAD_AFTER_UNREGISTERED;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.*;

@RunWith(JUnit4TestRunner.class)
public class ServiceProxyIntegrationTest {

    @Configuration
    public static Option[] configure() {
        return options(
                felix(),
                equinox(),
                provision(
                        mavenBundle().groupId("fr.insalyon.citi.osgi-substitution").artifactId("service-proxy").versionAsInProject()
                )
        );
    }

    @Test
    public void smoke() {
        assertThat(bundleContext, notNullValue());
        assertThat(serviceProxyBuilder, notNullValue());
    }

    @Inject
    BundleContext bundleContext = null;

    ServiceProxyBuilder serviceProxyBuilder;

    @Before
    public void setup() {
        ServiceReference reference = bundleContext.getServiceReference(ServiceProxyBuilder.class.getName());
        serviceProxyBuilder = (ServiceProxyBuilder) bundleContext.getService(reference);
    }

    @Test
    public void singlethread_proxy_kill_switch() {
        ServiceRegistration registration = bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);

        EchoService echoService = serviceProxyBuilder.getService(EchoService.class, DISABLED_AFTER_UNREGISTERED);
        assertThat(echoService, notNullValue());
        assertThat(echoService.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)echoService).isValid(), is(true));

        registration.unregister();
        try {
            echoService.echo("you must fail");
            throw new RuntimeException("Invoking the service should have thrown a UnregisteredServiceException");
        } catch (UnregisteredServiceException ignored) {
        }
        assertThat(((DynamicProxyReference)echoService).isValid(), is(false));
    }

    @Test
    public void multithread_proxy_kill_switch() {
        final ServiceRegistration registration = bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);

        EchoService echoService = serviceProxyBuilder.getService(EchoService.class, DISABLED_AFTER_UNREGISTERED);
        assertThat(echoService, notNullValue());
        assertThat(echoService.echo("hello"), is("hello"));

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(200);
                    registration.unregister();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        try {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                echoService.echo("hello");
            }
            throw new RuntimeException("Invoking the service should have thrown a UnregisteredServiceException");
        } catch (UnregisteredServiceException ignored) {
        }
        assertThat(((DynamicProxyReference)echoService).isValid(), is(false));
    }

    @Test
    public void reload_proxy_mode() {
        ServiceRegistration registration = bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);
        EchoService echoService = serviceProxyBuilder.getService(EchoService.class, RELOAD_AFTER_UNREGISTERED);

        assertThat(echoService, notNullValue());
        assertThat(echoService.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)echoService).isValid(), is(true));

        registration.unregister();
        try {
            echoService.echo("you must fail");
            throw new RuntimeException("Invoking the service should have thrown a UnregisteredServiceException");
        } catch (UnregisteredServiceException ignored) {
            ignored.printStackTrace();
        }
        assertThat(((DynamicProxyReference)echoService).isValid(), is(false));

        registration = bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);
        assertThat(echoService.echo("hello"), is("hello"));
        assertThat(((DynamicProxyReference)echoService).isValid(), is(true));
    }

    @Test
    public void service_broker() throws InvalidSyntaxException {
        ServiceRegistration firstReg = bundleContext.registerService(EchoService.class.getName(), new EchoServiceImpl(), null);
        ServiceRegistration secondReg = bundleContext.registerService(EchoService.class.getName(), new AnotherEchoServiceImpl(), null);
        ServiceRegistration funkyReg = bundleContext.registerService(List.class.getName(), new ArrayList(), null);

        ServiceBroker<EchoService> broker = serviceProxyBuilder.getServices(EchoService.class, null);

        Set<EchoService> services = broker.currentServices();
        assertThat(services, notNullValue());
        assertThat(services.size(), is(2));

        for (EchoService service : services) {
            assertThat(service.echo("plop"), is("plop"));
        }

        secondReg.unregister();
        funkyReg.unregister();

        try {
            for (EchoService service : services) {
                service.echo("plop");
            }
            throw new RuntimeException("Invoking the service should have thrown a UnregisteredServiceException");
        } catch (UnregisteredServiceException ignored) {
        }

        services = broker.currentServices();
        assertThat(services, notNullValue());
        assertThat(services.size(), is(1));

        for (EchoService service : services) {
            assertThat(service.echo("plop"), is("plop"));
            assertThat(((DynamicProxyReference)service).isValid(), is(true));
        }

        firstReg.unregister();

        services = broker.currentServices();
        assertThat(services, notNullValue());
        assertThat(services.size(), is(0));

        broker.discard();
    }
}
